### 结论
vue是异步渲染

举例：
往ul列表push3次li标签，如果在nextTick外面获取ul下的li标签是获取不到push的值，vue会整合修改集中处理

![](https://gitee.com/hellosanbao/mypic/raw/master/20210303153014.png)


### 为甚么要异步渲染

因为操作js消耗的性能比操作dom小得多，所以异步集中处理比改一下重新渲染一次性能好

