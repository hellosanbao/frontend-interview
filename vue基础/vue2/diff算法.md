- #### diff算法核心思想：只比较同层级，不跨级比较，这也是将时间复杂度从 o(n3)变成o(n)的关键
![](https://gitee.com/hellosanbao/mypic/raw/master/20210304112018.png)

- #### 当model改变产生新的vnode时，通过patch(oldVnode,vnode)来进行比较

```js
//简易过程，不是真实实现代码
//判断是否为相同节点的标砖，key相同且tag相同
function sameVnode(oldVnode, vnode){
    return vnode.key === oldVnode.key && vnode.sel === oldVnode.sel
}
function patch (oldVnode, vnode) {
    if (sameVnode(oldVnode, vnode)) {
        patchVnode(oldVnode, vnode) //如果当前层级节点相同，则比较他们的子节点
    } else {
        removeOldNode()  //删除旧节点
        insertNewNode()  //插入新节点
    }
    return vnode
}
```
- #### 当同层级节点不相同时，直接删除重建，若同层级节点相同，则调用patchVnode，对比children，patchVnode分4种情况

1）如果子节点是text节点，则对比新老text，不相等则把新的text节点设置到dom上

2）如果子节点是children节点，新老children都存在且不相等，则执行`updateChildren`，进行子节点的diff比较

3）如果老的children存在，新children不存在，则直接调用`removeChildren`删除老的children

4）如果老的children不存在，新的children存在，则`createEle`创建新的节点
```js
patchVnode (oldVnode, vnode) {
    const el = vnode.el = oldVnode.el
    let oldCh = oldVnode.children, ch = vnode.children
    //如果两个vnode相同，则直接return
    if (oldVnode === vnode) return

    //若vnode的text存在，则children就不存在，这时候如果新旧的text不相同，则直接将节点的text设置成新vnode的text
    if (oldVnode.text !== null && vnode.text !== null && oldVnode.text !== vnode.text) {
        api.setTextContent(el, vnode.text)
    }else {
      if (oldCh && ch && ch !== oldCh) {
            //如果存在children且oldchildren 与newChildren不相等，执行updateChildren，进行子节点的diff比较
            updateChildren(el, oldCh, ch)
        }else if (ch){
            //如果newChildren存在，oldChildren不存在，则创建新的children
            createEle(vnode)
        }else if (oldCh){
            //如果newChildren不存在，oldChildren存在，则直接删除oldChildren
            api.removeChildren(el)
        }
    }
}

```




- #### 经典updateChildren具体过程（面试不一定问）
![](https://gitee.com/hellosanbao/mypic/raw/master/20210304140409.png)

过程可以概括为：oldCh和newCh各有两个头尾的变量StartIdx和EndIdx，它们的2个变量相互比较，一共有4种比较方式。如果4种比较都没匹配，如果设置了key，就会用key进行比较，在比较的过程中，变量会往中间靠，一旦StartIdx>EndIdx表明oldCh和newCh至少有一个已经遍历完了，就会结束比较。


```js
updateChildren (parentElm, oldCh, newCh) {
    let oldStartIdx = 0, newStartIdx = 0
    let oldEndIdx = oldCh.length - 1
    let oldStartVnode = oldCh[0]
    let oldEndVnode = oldCh[oldEndIdx]
    let newEndIdx = newCh.length - 1
    let newStartVnode = newCh[0]
    let newEndVnode = newCh[newEndIdx]
    let oldKeyToIdx
    let idxInOld
    let elmToMove
    let before
    while (oldStartIdx <= oldEndIdx && newStartIdx <= newEndIdx) {
            if (oldStartVnode == null) {   //对于vnode.key的比较，会把oldVnode = null
                oldStartVnode = oldCh[++oldStartIdx] 
            }else if (oldEndVnode == null) {
                oldEndVnode = oldCh[--oldEndIdx]
            }else if (newStartVnode == null) {
                newStartVnode = newCh[++newStartIdx]
            }else if (newEndVnode == null) {
                newEndVnode = newCh[--newEndIdx]
            }else if (sameVnode(oldStartVnode, newStartVnode)) {
                //新老startVnode对比，相同，则startIdx各加1，同时执行patchVnode，进行子节点diff
                patchVnode(oldStartVnode, newStartVnode)
                oldStartVnode = oldCh[++oldStartIdx]
                newStartVnode = newCh[++newStartIdx]
            }else if (sameVnode(oldEndVnode, newEndVnode)) {
              //新老endVnode对比，相同，则endIdx各减1，同时执行patchVnode，进行子节点diff
                patchVnode(oldEndVnode, newEndVnode)
                oldEndVnode = oldCh[--oldEndIdx]
                newEndVnode = newCh[--newEndIdx]
            }else if (sameVnode(oldStartVnode, newEndVnode)) {
                //旧startVnode对比新endVnode，相同则oldstartIdx加1，newEndIdx减1
                patchVnode(oldStartVnode, newEndVnode)
                api.insertBefore(parentElm, oldStartVnode.el, api.nextSibling(oldEndVnode.el))
                oldStartVnode = oldCh[++oldStartIdx]
                newEndVnode = newCh[--newEndIdx]
            }else if (sameVnode(oldEndVnode, newStartVnode)) {
              //旧endVnode对比新startVnode，相同则newstartIdx加1，oldEndIdx减1
                patchVnode(oldEndVnode, newStartVnode)
                api.insertBefore(parentElm, oldEndVnode.el, oldStartVnode.el)
                oldEndVnode = oldCh[--oldEndIdx]
                newStartVnode = newCh[++newStartIdx]
            }else {
                // 若以上几种都没匹配到
               // 使用key时的比较
                if (oldKeyToIdx === undefined) {
                  //生成oldChildren的index和key的对应表
                  /**
                   * 结果大致是
                   * {
                   *  keyname:index
                   * }
                   * 
                  */
                    oldKeyToIdx = createKeyToOldIdx(oldCh, oldStartIdx, oldEndIdx)

                }
                idxInOld = oldKeyToIdx[newStartVnode.key]
                  //查找新的newStartVnode的key是否能在oldCh中匹配到
                if (!idxInOld) {
                    //若没有匹配到，则判定为新增的vnode，直接插入，newStartIdx加1
                    api.insertBefore(parentElm, createEle(newStartVnode).el, oldStartVnode.el)
                    newStartVnode = newCh[++newStartIdx]
                }
                else {
                  //如果匹配到了，则不是新增的vnode，newStartIdx加1
                    elmToMove = oldCh[idxInOld]
                    if (elmToMove.sel !== newStartVnode.sel) {
                        //如果当前index对应的新老节点不一致，则将新节点插入oldStartVnode和newStartVnode之间
                        api.insertBefore(parentElm, createEle(newStartVnode).el, oldStartVnode.el)
                    }else {
                        //如果一致，则调用patchVnode进行子节点的diff比较
                        patchVnode(elmToMove, newStartVnode)
                        oldCh[idxInOld] = null
                        api.insertBefore(parentElm, elmToMove.el, oldStartVnode.el)
                    }
                    newStartVnode = newCh[++newStartIdx]
                }
            }
        }
        if (oldStartIdx > oldEndIdx) {
            before = newCh[newEndIdx + 1] == null ? null : newCh[newEndIdx + 1].el
            addVnodes(parentElm, before, newCh, newStartIdx, newEndIdx)
        }else if (newStartIdx > newEndIdx) {
            removeVnodes(parentElm, oldCh, oldStartIdx, oldEndIdx)
        }
}

```


方便理解，贴一张网图
![](https://gitee.com/hellosanbao/mypic/raw/master/20210304142623.png)