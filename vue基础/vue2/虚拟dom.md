### 描述
vue的虚拟dom就是用来描述dom结构的json对象

### 为什么用vdom
通过数据模型连修改vdom数据，计算出最小差异再来统一操作真实dom，节省性能

例如：
> dom
```html
<div class="box" id="app">
  <p>text</p>
  <ul>
    <li style="color:red;">li-test</li>
  </ul>
</div>

```

> vdom

```js
{
  tag:'div',
  props:{
    class:'box',
    id:'app'
  },
  children:[
    {
      tag:'p',
      children:'text'
    },
    {
      tag:'ul',
      children:[
        {
          tag:'li',
          props:{
            style:'color:red;'
          },
          children:'li-test'
        }
      ]
    }
  ]
}
```