- 合理使用v-show和v-if

- 合理使用computed

- v-for时加key，避免v-if和v-show同时使用

- 合理使用keep-alive

- 合理使用异步组件

- 自定义事件、dom事件及时销毁

- data层级不要太深
 