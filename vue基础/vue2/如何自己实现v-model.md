```vue
<template>
  <input type="text" :value="txt" @input="$emit('change'),$event.target.value"/>
</template>

<script>
  export default {
    model:{
      prop:'txt',
      event:'change'
    },
    props:{
      txt:String
    }
  }
</script>
```
