### 结论
通过`Object.defineProperty`来监听data的`getter`和`setter`，从而实现视图的渲染和更新

### 缺点
- `Object.defineProperty`无法监听数组,vue通过重写数组的push、pop、splice等方法实现数组的监听（所以直接通过数组下标的操作，vue是监听不到的）
- 复杂对象深度监听的时候，需要递归，计算量大
- `Object.defineProperty`无法监听data新增属性、删除属性（vm.$set,vm.$delete来实现）

### 例子
```js
//更新视图
function updateview(){
  console.log('更新视图')
}

//重写数组原型方法
function observeArray(target){
  const originArrayPrototype = Array.prototype
  //创建一个对象，原型指向originArrayPrototype，这样修改cpProto就不会影响originArrayPrototype
  const cpProto = Object.create(originArrayPrototype)

  const funcMap = ['push','pop','shift','unshift','splice']
  funcMap.froEach((method)=>{
    cpProto[method] = function(){
      updateView()
      originArrayPrototype[method].call(this,...arguments)
    }
  })
  target.__proto__ = cpProto
}

//定义监听属性
function defineReactive(target,key,value){
  //深度监听
  observer(value)

  Object.defineProperty(target,key,{
    get(){
      return value
    },
    set(newValue){
      if(newValue !== value){
        //当设置新的对象是，将新设置的对象深度监听
        observe(newValue)
        value = newValue
        //更新视图
        updateView()
      }
    }
  })
}

//监听对象
function observer(target){
  if(typeof target !== 'object' || target === null){
    return target
  }

  if(Array.isArray(target)){
    observeArray(target)
  }

  for(let key in target){
    defineReactive(target,key,target[key])
  }
}

```