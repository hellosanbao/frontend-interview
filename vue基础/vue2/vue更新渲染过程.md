## 初次渲染
- 1：解析模板为render函数（或在开发环境完成，vue-loader）
- 2：触发响应式，监听data属性发热getter和setter（初次渲染一般只触发getter）
- 3：执行render函数，生成vnode，patch(elem,vnode)


 ## 更新过程：
 - 1、修改data,触发setter（此前在getter中已被监听）
 - 2、重新执行render函数，生成newVnode
 - 3、patch(vnode,newVnode)
 
![](https://gitee.com/hellosanbao/mypic/raw/master/WX20210303-150909@2x.png)
