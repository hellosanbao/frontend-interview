- ### 不可变值
要遵循不可变值原则(不直接修改state的值)，否则性能优化的时候(scu)会有问题

- ### 可能是异步更新
在react的生命周期中，setState是异步的
在setTimeout、自定义dom事件中，setState是同步的
- ### 可能会合并
传入对象会被合并，传函数不会合并
```js

//会被合并
this.setState({
  count:this.state.count+1
})
this.setState({
  count:this.state.count+1
})
this.setState({
  count:this.state.count+1
})

//不会被合并
this.setState((prevState,props)=>{
  return {
    count:prevState.count + 1
  }
})
this.setState((prevState,props)=>{
  return {
    count:prevState.count + 1
  }
})
this.setState((prevState,props)=>{
  return {
    count:prevState.count + 1
  }
})

```

