### ref作用
用来获取react Dom实例

### 如何使用

```js
//函数方式
render(){
  return <div ref={(e)=>this.testRef = e}></div>
}

//createRef
constructor(){
  this.testRef = React.createRef()
}
render(){
  return <div ref={this.testRef}></div>
}

```