### 常用内置hooks
- useState
- useEffect
- useCallback
- useMemo
- useRef
- useContext
- useReducer


- #### useState
```js
import React, {useState} from 'react'
const AddCount = () => {
  const [ count, setCount ] = useState(0)
  const addcount = () => {
    let newCount = count
    setCount(newCount+=1)
  } 
  return (
    <>
      <p>{count}</p>
      <button onClick={addcount}>count++</button>
    </>
  )
```

- #### useEffect
```js
//第一次渲染之后和每次更新之后都会执行
useEffect(()=>{
  console.log('第一次渲染之后和每次更新之后都会执行')
})

//首次加载和卸载执行
useEffect(()=>{
  console.log('首次加载')
  return function(){
    console.log('卸载')
  }
},[])

//当依赖的值发生变化的时候执行
useEffect(()=>{
  console.log('count发生变化的时候执行')
},[count])

```

- #### useCallback
会缓存传入的函数，只有当依赖变化的时候，函数才会更新
```js
import { useCallback, useState } from 'react'
function Tset(){
  const [t,st] = useState(0)
  let a = t
  const cb = useCallback((...args)=>{
    console.log(a) //a只会是0,除非将a添加依赖
  },[])

  return(
    <div>
      <button onClick={()=>{st(t+1)}}>change state</button>
    </div>
  )
}
```

- #### useRef


- #### useContext
```js
import { createContext, useContext } from 'react'

const AppContent = createContext({})

function Child(){
  const {test,setTest} = useContext(AppContent)
  return <div onClick={()=>setTest('newTextValue')}>{test}</div>
}

function Parent(){
  const [test,setTest] = useState(0)
  return (
     <AppContent.Provider value={{test:'testValue',setTest}}>
      <Child/>
     <AppContent.Provider>
  )
}

```


- #### useReducer

```js
import {useReducer} from 'react' 
const store = {
  num:10
}

const reducer = (state,action) => {
  switch (action.type) {
    case 'changeNum':
      return {
        ...state,
        num:action.num
      }
    default:
      return {
        ...state
      }
  }
}

function TextComponent(){
  const [state,dispatch] = useReducer(reducer,store)

  return (
    <div onClick={()=>dispatch({type:'changeNum',num:100})}>
      {state.num}
    </div>
  )
}
```

