当props变化的时候执行

componentWillReceiveProps在初始化render的时候不会执行，它会在Component接受到新的状态(Props)时被触发，一般用于父组件状态更新时子组件的重新渲染

```js
componentWillReceiveProps(nextProps)
```