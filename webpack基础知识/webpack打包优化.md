### 减少loader的处理范围
```js
module.exports = {
  module:{
    rules: [
      {
        test:'/\.js$/',
        loader:'babel-loader',
        include:[resolve('src')], //只处理src下的文件
        exclude:'/node_modules/', //忽略node_modules下的文件
      }
    ]
  }
}
```

### 可以将babel编译过的文件缓存

```js
loader:'babel-loader?cacheDirectory=true'
```

### happyPack
因为受限于Node的单线程运行，所以webpack的打包也是单线程的，使用HappyPack可以将Loader的同步执行转为并行，从而执行Loader时的编译等待时间

```js
module.exports = {
  module:{
    rules: [
      {
        test:'/\.js$/',
        loader:'happypack/loader?id=happybabel',
        include:[resolve('src')], //只处理src下的文件
        exclude:'/node_modules/', //忽略node_modules下的文件
      }
    ]
  },
  plugins:[
    new Happypack({
      id:'happybabel',
      loaders:['babel-loader?cacheDirectory=true'],
      threads:4, //线程开启数
    })
  ]
}
```


### DllPlugin

该插件可以将特定的类库提前打包然后引入，这种方式可以极大的减少类库的打包次数，只有当类库有更新版本时才会重新打包，并且也实现了将公共代码抽离成单独文件的优化方案。

```js
// webpack.dll.conf.js
const path = require('path')
const webpack = require('webpack')
module.exports = {
    entry: {
        vendor: ['react'] // 需要统一打包的类库
    }，
    output: {
      path: path.join(__dirname, 'dist'),
        filename: '[name].dll.js',
        library: '[name]-[hash]'
    },
    plugins: [
        new webpack.DllPlugin({
            name: '[name]-[hash]', //name必须要和output.library一致
            context: __dirname, //注意与DllReferencePlugin的context匹配一致
            path: path.join(__dirname, 'dist', '[name]-manifest.json')
        })
    ]
}
```
然后在package.json文件中增加一个脚本

```js
'dll': 'webpack --config webpack.dll.js --mode=development'
//运行后会打包出react.dll.js和manifest.json两个依赖文件
```

最后使用DllReferencePlugin将刚生成的依赖文件引入项目中

```js
// webpack.conf.js
module.exports = {
    //...其他配置
    plugins: [
        new webpack.DllReferencePlugin({
            context: __dirname,
            manifest: require('./dist/vendor-manifest.json') //此即打包出来的json文件
        })
    ]
}
```

### 减少打包大小

1.按需加载，首页加载文件越小越好，将每个页面单独打包为一个文件，（同样对于loadsh类库也可以使用按需加载），原理即是使用的时候再去下载对应的文件，返回一个promise，当promise成功后再去执行回调。


### Tree shaking
它会删除项目中未被引用的代码，而如果在webpack 4中只要开启生产环境就会自动启动这个功能