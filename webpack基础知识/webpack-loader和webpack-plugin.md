# webpack loader
loader本质上是一个内容转换器，将输入的内容经过加工、翻译成需要的结果输出，比如'sass-loader'是将scss文件内容转换成css，'vue-loader'是将vue文件内容转换成样式表和creatElement函数等

### 常见loader
> 语言转换类
- babel-loader:把ES6转成ES5
- ts-loader:把TypeScript转成JavaScript
- sass-loader:把scss/sass转成css
- less-loader:把less代码转成css
- css-loader:加载css，文件导入等

> 其他
- file-loader:将文件输出到一个文件夹中，使用相对路径引用输出文件
- source-map-loader:加载额外的SourceMap文件，方便断点调试
- vue-loader:加载.vue文件
- i18n-loader:加载多语言版本
- ui-component-loader:按需加载组件库
- ignore-loader:忽略部分文件


### 配置loader

```js
module.exports = {
  module:{
    rules:[
      {
        test:'/\.scss/',
        use:[
          {
            loader:'sass-loader',
            option:{
              minimize:true
            }
          }
        ]
      }
    ]
  }
}
```

### 编写一个loader

```js
/**
 *laoder本质是一个函数
 * @param {string|Buffer} content 源文件的内容
 * @param {object} [map] 可以被 https://github.com/mozilla/source-map 使用的 SourceMap 数据
 * @param {any} [meta] meta 数据，可以是任何内容
 */
module.exports = function testLoader(content,sourceMap){
  console.log('context',this.context);
  console.log('resourcePath',this.resourcePath);
  console.log('getOptions',this.getOptions())

  //异步处理可以用this.async
  const asyncFunc = this.async()

  setTimeout(()=>{
    asyncFunc(null,content,sourceMap) //第一个参数是error内容
  },1000)

  // 同步处理可以直接return一个字符串、或者是多个处理结果的时候需要用this.callback
  // return content+'loader test string'
  //or
  //this.callback(null,content,sourceMap)
}

```




# webpack plugin