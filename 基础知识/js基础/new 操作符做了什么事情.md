- 创建一个新对象
- 将对象的__proto__赋值成构造函数的prototype
- 调用构造函数，并将构造函数的this替换成创建的对象
- 如果构造函数返回值为对象或者函数，则返回该对象或者函数，否则返回创建的新对象obj

```js
function Person(name,age){
   this.name = name ;
   this.age = age ;
}
function rewriteNew(sup,...param){
  //1.创建新对象
  var obj = {};
  //2.将空对象作为 this，调用构造函数并传入参数
  var result = sup.call(obj,...param);
  //3.将该对象__proto__指向构造函数原型对象，实现继承
  obj.__proto__ = sup.prototype;//Object.setPrototypeOf(obj,sup.prototype)
  //4.如果构造函数返回值为对象或者函数，则返回该对象或者函数，否则返回创建的新对象obj
  return  typeOf result === "object" || typeOf result === "function"?result:obj
};
rewriteNew(Person,'baoye',18)

```