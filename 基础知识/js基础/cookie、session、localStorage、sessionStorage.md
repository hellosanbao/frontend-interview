### cookie简介
- cookie数据存放在客户的浏览器上
- cookie会在请求的时候自动带到请求头上
- cookie中保存的是字符串
- 同一个域名下，cookie可以设置path来限制是否能被访问
- 最大容量 4kb
- 浏览器端用`document.cookie= 'cookiename=xxx'`来设置(不支持一次写入多个)
- 服务端通过响应头上添加`Set-Cookie：cookiename=xxx`来设置
>  cooke格式
![](https://gitee.com/hellosanbao/mypic/raw/master/20210317161842.png)


### session简介
- session数据放在服务器上
- session中保存的是对象
- 无大小限制


### localStorage
- 存在浏览器端
- 大小限制为5M
- 无时间限制
- 值类型为字符串

### sessionStorage
- 与localstorage类似，区别是仅在当前浏览器窗口中有效，关闭窗口即清除


### cookie、sessionStorage和localStorage的区别
- cookie会在请求时自动带到请求头上，sessionStorage和localStorage不会
- cookie大小4k，sessionStorage和localStorage一般有5M
- localStorage不会过期、sessionStorage在窗口关闭后失效、只在设置的cookie过期时间之前有效

