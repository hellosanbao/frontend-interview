### Map
Map是一组键值对的结构，具有极快的查找速度。

要创建一个Map，需要提供一个二维数组作为输入，或者直接创建一个空Map

```js
//初始化空的map，通过api赋值
var map1 = new Map()
map1.set('age',18)
map1.set('name','小明')
console.log(map1) //Map(2) {"age" => 18, "name" => "小明"}
console.log(map1.set('name')) //小明
map1.delete('name')  //删除name


//传入二维数组，赋默认值
var map2 = new Map([['name','小明'],['age',18]])
console.log(map2)//Map(2) {"age" => 18, "name" => "小明"}
```


### Set
Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。

要创建一个Set，需要提供一个Array作为输入，或者直接创建一个空Set

```js
var s1 = new Set(); // 空Set
s1.add(1)  //添加1
s1.delete(1) //删除1
var s2 = new Set([1, 2, 3]); // 含1, 2, 3


//用于数组去重
var s3 = new Set([1,2,2,3,4,5,5,6])
Array.from(s3)  //[1,2,3,4,5,6]
```