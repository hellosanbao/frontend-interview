#### call
利用对象方式调用函数的特性，将原函数赋值到劫持对象上调用，即可改变函数的this指向
```js
Function.prototype.myCall = function(_this,...args){
  switch (typeof _this) {
    case 'string':
      _this = new String(_this)
      break;
    case 'number':
      _this = new Number(_this)
      break;
    case 'boolean':
      _this = new Boolean(_this)
      break;
    default:
      break;
  }
  const Func = this
  const fnName = Symbol()
  _this[fnName] = Func
  _this[fnName](...args)
  delete _this[fnName]
}

function aaa(aaa){
  console.log(this,aaa)
}
aaa.call({name:'test'},'hello')

```

#### apply 
与call方法实现原理一致，只是参数接收变为数组
```js
Function.prototype.myApply = function(_this,args=[]){
  switch (typeof _this) {
    case 'string':
      _this = new String(_this)
      break;
    case 'number':
      _this = new Number(_this)
      break;
    case 'boolean':
      _this = new Boolean(_this)
      break;
    default:
      break;
  }
  const Func = this
  const fnName = Symbol()
  _this[fnName] = Func
  _this[fnName](...args)
  delete _this[fnName]
}

function aaa(aaa){
  console.log(this,aaa)
}
aaa.call({name:'test'},'hello')

```

#### bind
```js
 Function.prototype.mybind= function(_this){
    const Func = this
    switch (typeof _this) {
      case 'string':
        _this = new String(_this)
        break;
      case 'number':
        _this = new Number(_this)
        break;
      case 'boolean':
        _this = new Boolean(_this)
        break;
      default:
        break;
    }
    return function(...args){
      Func.myApply(_this,args)
    }
  }
  function aaa(aaa){
    console.log(this,aaa)
  }
  aaa.mybind('123')('hello')
```