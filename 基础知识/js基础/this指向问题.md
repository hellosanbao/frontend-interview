### 描述

this的值取决于函数调用的时候，与函数定义无关

### 主要场景

- #### 直接调用的函数，this指向window，箭头函数除外，例如

```js
function fn1(){
  console.log(this)  
}
fn1() //window


var obj = {
  test(){
    function fn2(){
      console.log(this)
    }
    fn2()
  }
}
obj.test() //window
```

- ####  函数作为对象的方法调用，this指向调用该方法的对象， 例如

```js
var obj = {
  test(){
    console.log(this)
  }
}
obj.test() //obj

function fn1(){
  console.log(this)  
}
obj.aaa = fn1
obj.aaa()//obj

class A{}

A.aaa = ()=>{console.log(this)} 
A.aaa() // class A

```

- #### 函数使用call、apply、bind的方式调用，this指所传入的值(若未传值，则糊略call、apply、bind，按照其他规则)， 例如

```js
function fn1 (){
  console.log(this)
}
fn1.call('hello')  //String {"hello"}

fn1.call() //window

fn1.call({name:1}) //{name:1}
```

- #### 箭头函数，this指向当前所在作用域的this，若当前为全局作用域，则指向window，例如：

```js
let fn1 = ()=>{ console.log(this) }
fn1() //window

var obj = {
  test:()=>{
    console.log(this)
  },
  test2(){
    let fn2 = ()=>{
      console.log(this)
    }
    fn2()
  }
}
obj.test() //window
obj.test2() //obj


```



