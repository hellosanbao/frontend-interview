> number、 string、 function、 object、 undefined、 symbol

```js
typeof 1 // 'number'
typeof '1' // 'string'
typeof function(){} // 'function'
typeof {}  // 'object'
typeof [] // 'object'
typeof null // 'object'
typeof undefined // 'undefined'
typeof Symbol() // '"symbol"'
```