## 宏任务

- script 脚本
- setTimeout
- setInterval
- ajax
- DOM 事件
- I/O

## 微任务

- Promise
- MutationObserver（Mutation Observer API 用来监视 DOM 变动）

## event loop

1. 首先执行 `script`，`script` 被称为全局任务，也属于 `macrotask`;
2. 当 `macrotask` 执行完以下，执行所有的微任务；
3. 微任务全部执行完，再取任务队列中的一个宏任务执行。

## 练习题

```js
console.log('start')
setTimeout(() => {
  console.log('setTimeout1')
})
new Promise((resolve) => {
  console.log('Promise1')
  resolve()
}).then(() => {
  console.log('then1')
  new Promise((resolve) => {
    resolve()
  }).then(() => {
    console.log('then2')
  })
  setTimeout(() => {
    console.log('setTimeout2')
  })
})
// 结果start Promise1 then1 then2 setTimeout1 setTimeout2
```

```js
async function async1() {
  console.log('asyncStart1')
  await async2()
  console.log('asyncEnd1')
}
async function async2() {
  return Promise.resolve().then((res) => {
    console.log('asyncPromise2')
  })
}
console.log('start')
setTimeout(() => {
  console.log('setTimeout')
}, 0)
async1()
new Promise((resolve) => {
  console.log('promise1')
  resolve()
}).then(() => {
  console.log('promise2')
})
// 结果 start asyncStart1  promise1 asyncPromise2 promise2 asyncEnd1 setTimeout
```
