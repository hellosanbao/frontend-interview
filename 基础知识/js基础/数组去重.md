### Set去重
```js
let set = new Set(arr)
Array.from(set)
```

### indexOf去重

```js
var newArr = [];
for (var i = 0; i < arr.length; i++) {
    if (newArr.indexOf(arr[i]) === -1) {
        newArr.push(arr[i])
    }
}
```


<!-- ### 哈希去重

```js
var newArr = [];
var obj = {}
for (var i = 0; i < arr.length; i++) {
    if (obj[typeof arr[i] + arr[i]] == undefined) {
        newArr.push(arr[i]);
        obj[typeof arr[i] + arr[i]] = true;
    }
}

``` -->

### include

```js
var newArr=[];
for(var i in arr){
    if(!newArr.includes(arr[i])){
        newArr.push(arr[i])
    }
}
```

### map

```js
 const map = new Map();
 arr.filter((item)=>!map.has(item)&&map.set(item,1))

```