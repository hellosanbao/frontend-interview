## promise基本使用

```js
promise
.then(
  function resolveHandle(result){console.log('resolve执行')},
  function rejectHandle(result){console.log('reject执行')}
)
.catch((err)=>{
  console.log('reject执行')
})
.finally(()=>{
  console.log('不管是resolve还是reject都会执行finally')
})
```


## 手写promise
```js
class MPromise {
  static PENDING = "pending";
  static RESOLVED = "resolved";
  static REJECTED = "rejected";
  static resolve = function(result){
    return new MPromise(resolve=>{
      resolve(result)
    })
  }
  static reject = function(result){
    return new MPromise(reject=>{
      reject(result)
    })
  }
  static all = function(promiseArr){
    return new MPromise((resolve)=>{
      let count = 0
      let resArr = new Array(promiseArr.length)
      promiseArr.forEach((promise,index)=>{
        promise.then(
          (res)=>pushPromiseArrHandle(res,index),
          (res)=>pushPromiseArrHandle(res,index)
        )
      })
      function pushPromiseArrHandle(result,index){
        count += 1
        resArr[index] = result
        if(count === promiseArr.length){
          resolve(resArr)
        }
      }
    })
  }
  constructor(func) {
    this.status = MPromise.PENDING
    this.result = null
    this.resolveCallBacks = []
    this.rejectCallBacks = []
    try {
      func(this.resolve.bind(this), this.reject.bind(this));
    } catch (error) {
      this.reject(error)
    }
  }
  resolve(result) {
    if(this.status === MPromise.PENDING){
      this.status = MPromise.RESOLVED
      this.result = result
      this.resolveCallBacks.forEach(resolveFunc=>{
        if(typeof resolveFunc === 'function'){
          resolveFunc(this.result)
        }
      })
    }
  }
  reject(result) {
    if(this.status === MPromise.PENDING){
      this.status = MPromise.REJECTED
      this.result = result
      this.rejectCallBacks.forEach(rejectFunc=>{
        if(typeof rejectFunc === 'function'){
          rejectFunc(this.result)
        }
      })
    }
  }
  then(onResolveFun,onRejectFun) {
    return new MPromise((resolve,reject)=>{
      const onResolveAgin = (result)=>{
        resolve(onResolveFun(result))
      }
      const onRejectAgin = (result)=>{
        reject(onRejectFun(result))
      }
      if(this.status === MPromise.PENDING){
        this.resolveCallBacks.push(onResolveAgin)
        this.rejectCallBacks.push(onRejectAgin)
      }
      if(this.status === MPromise.RESOLVED && typeof onResolveFun === 'function'){
        onResolveAgin(this.result)
      }
      if(this.status === MPromise.REJECTED && typeof onRejectFun === 'function'){
        onRejectAgin(this.result)
      }
    })
  }
}







 let m1 = new MPromise((resolve,reject)=>{
  setTimeout(()=>{
    resolve('resolve:这次一定')
  },3000)
})
let m2 = new MPromise((resolve,reject)=>{
  setTimeout(()=>{
    reject('reject:下次一定')
  },2000)
})

MPromise.all([m1,m2]).then(res=>{
  console.log(res);
})

```