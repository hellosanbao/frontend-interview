```js
var arr = [1,2,3,[4,5,6,[7,8,9,[10,11,12]]]]
Array.prototype.flat = function(){
  return this.reduce((total,current)=>{
    if(Array.isArray(current)){
      total = total.concat(current.flat())
    }else{
      total.push(current)
    }
    return total
  },[])
}
console.log(arr.flat())
```