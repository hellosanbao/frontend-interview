### BFC是什么
块级格式化上下文

### BFC布局规则是
- 内部的Box会在垂直方向，一个接一个地放置
- Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠
- BFC的区域不会与float box重叠。
- BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
- 计算BFC的高度时，浮动元素也参与计算

### 哪些元素会生成BFC
- float不为none
- position不为static/relative
- display的值为inline-block、table-cell、table-caption
- overflow的值不为visible
- 根元素

### BFC的使用场景有哪些
- 自适应两栏布局
- 清除内部浮动
- 防止垂直 margin 重叠