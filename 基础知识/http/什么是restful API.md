![](https://gitee.com/hellosanbao/mypic/raw/master/20210311135619.png)


> 例如
#### 传统API
```js
// http://www.baidu.com/info?id=3
```
#### restful API
```js
// http://www.baidu.com/info/3
```

restful API原则

- 尽量不用url参数

- 用method表示操作类型
