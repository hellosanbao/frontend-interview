### 三次握手
- 第一次握手：客户端发送一个带SYN的TCP报文到服务器，表示客户端想要和服务器端建立连接。
- 第二次握手：服务器端接收到客户端的请求，返回客户端报文，这个报文带有SYN和ACK确认标示，访问客户端是否准备好。
- 第三次握手：客户端再次响应服务端一个ACK确认，表示我已经准备好了。
![](https://gitee.com/hellosanbao/mypic/raw/master/20210321174628.png)

### 四次挥手
- 第一次挥手：TCP发送一个FIN(结束)，用来关闭客户端到服务器端的连接。
- 第二次挥手：服务器端收到这个FIN后发回一个ACK确认标示，确认收到。
- 第三次挥手：服务器端发送一个FIN到客户端，服务器端关闭客户端的连接。
-  第四次挥手：客户端发送ACK报文确认，这样关闭完成。
![](https://gitee.com/hellosanbao/mypic/raw/master/20210321174753.png)


### 考题
- #### 为什么使用三从握手
> 为了防止已失效的连接请求报文段突然又传送到了服务端，因而产生错误

例如：client发出的第一个连接请求报文段并没有丢失，而是在某个网络结点长时间的滞留了，以致延误到连接释放以后的某个时间才到达server。本来这是一个早已失效的报文段。但server收到此失效的连接请求报文段后，就误认为是client再次发出的一个新的连接请求。于是就向client发出确认报文段，同意建立连接。假设不采用“三次握手”，那么只要server发出确认，新的连接就建立了。由于现在client并没有发出建立连接的请求，因此不会理睬server的确认，也不会向server发送数据。但server却以为新的运输连接已经建立，并一直等待client发来数据。这样，server的很多资源就白白浪费掉了

- #### 为甚么挥手比握手多一次
关闭连接时，当Server端收到FIN报文时，很可能并不会立即关闭SOCKET，

所以只能先回复一个ACK报文，告诉Client端，"你发的FIN报文我收到了"。

只有等到我Server端所有的报文都发送完了，我才能发送FIN报文，因此不能一起发送。

故需要四步握手。