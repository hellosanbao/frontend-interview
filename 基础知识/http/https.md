### 描述
HTTPS协议是由SSL+HTTP协议构建的可进行加密传输、身份认证的网络协议，要比http协议安全


### HTTP与HTTPS有什么区别

- https协议需要到ca申请证书，一般免费证书较少，因而需要一定费用
- http是明文传输，https则是具有安全性的ssl加密传输协议。
- http和https使用的是完全不同的连接方式，用的端口也不一样，前者是80，后者是443。
- http的连接很简单，是无状态的；HTTPS协议是由SSL+HTTP协议构建的可进行加密传输、身份认证的网络协议，比http协议安全。

### HTTPS的工作原理
![](https://www.mahaixiang.cn/uploads/allimg/1507/1-150H120343I41.jpg)

- 客户端发起请求，要求与服务器建立ssl连接
- 服务器收到后返回网站的证书信息（包含公钥）
- 客户端使用返回的公钥对将客户端的私钥加密传给服务端
- 然后就可以使用客户端的密钥对会话进行加密通信

![](https://pic002.cnblogs.com/images/2012/339704/2012071410212142.gif)