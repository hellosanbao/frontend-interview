### Request Headers
- `Accept` 浏览器可接收的数据格式
- `Accept-Encoding` 浏览器可接收的压缩算法,如 gzip
- `Accept-Language` 浏览器可接收的语言，如zh-CN
- `connection:keep-alive` 一次tcp连接重复使用
- `cookie` 浏览器自动会将本地cookie带上
- `Host` 请求的ip、域名
- `User-Agent` 浏览器信息
- `Content-Type` 发送数据的格式，如application/json
- `Catch-Control、Expires`: 本地缓存过期时间
- `If-Modified-Since`: 服务器下发的文件最后修改时间,回传给服务器对比
- `If-None-Match`: 服务器下发的资源标志串,回传给服务器对比

### Response Headers

- `Content-Type`: 返回的数据的格式，如application/json
- `Content-Length`: 返回的数据大小，多少字节
- `Content-Encoding`: 返回数据的压缩算法，如gzip
- `Set-Cookie`: 服务端往客户端设置cookie
- `Last-Modified`: 服务器下发的文件最后修改时间
- `Etag`: 服务器下发的资源标志串