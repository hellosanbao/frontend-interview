### 强缓存

![](https://gitee.com/hellosanbao/mypic/raw/master/20210310165226.png)

#### catch-control
> 取值

- max-age  缓存时间

- no-catch 不用强制缓存，是否缓存取决于服务端

- no-store  不用强制缓存，也不需要服务端做缓存

初次请求，服务端会返回资源和catch-control(如：catch-control:max-age=36000)，浏览器接收到catch-control后，会将资源缓存到本地，下次在请求改资源，浏览器会根据catch-control判断缓存是否过期，如果没有过期，则直接去本地缓存的资源返回


### Expires 
与catch-control功能类似，属于老的规范，已被catch-control代替，如果同时存在，浏览器会优先采用catch-control


### 协商缓存

#### Etag

![](https://gitee.com/hellosanbao/mypic/raw/master/20210311134039.png)

首次请求的时候，服务端会返回和`Etag`,下次请求的时候，请求头会带上`If-None-Match`,值就是上次返回的etag的值，服务端收到后判断是否匹配，如匹配成功则返回304，浏览器走本地缓存，若没有匹配到则返回资源和新的`Etag`



#### Last-Modified

![](https://gitee.com/hellosanbao/mypic/raw/master/20210311112654.png)

首次请求的时候，服务端会返回和`last-modified`,下次请求的时候，请求头会带上`If-Modified-Since`,值就是上次返回的last-modified的值，服务端收到后判断是否匹配，如匹配成功则返回304，浏览器走本地缓存，若没有匹配到则返回资源和新的`last-modified`

