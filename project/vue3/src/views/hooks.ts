import { Ref, ref } from "vue";
interface TestHookInstance {
  testName: Ref<string>;
  changeTestName: (name: string) => void;
}

export function useTestHooks(): TestHookInstance {
  const testName = ref("testName");

  function changeTestName(name: string) {
    console.log(name);

    testName.value = name;
  }

  return {
    testName,
    changeTestName,
  };
}
