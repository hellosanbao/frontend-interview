import { useMemo } from 'react'
import { useContext, useCallback } from 'react'
import useMytesthooks from './hooks/useMytesthooks'
import AppContext from './reducer/index'

const TestComponent = () => {
  const { storeData, dispatch } = useContext(AppContext)
  const {a,setA} = useMytesthooks()

  const testCallback = useCallback((a,b)=>{
    console.log(a,b,storeData)
  },[storeData])

  const changeNum = () => {
    dispatch({
      type: 'changeCount',
      num: storeData.num + 1
    })
  }

  const memoData = useMemo(()=>{
    return a+'-memo'
  },[a])

  return (
    <>
      <div onClick={changeNum} >{storeData.num}</div>
      <div onClick={()=>{testCallback('a','b')}} >测试useCallback</div>
      <div onClick={()=>{setA('change-a')}} >测试自定义Hooks---{a}</div>
      <div >usememo -- {memoData}</div>
    </>
  )
}

export default TestComponent