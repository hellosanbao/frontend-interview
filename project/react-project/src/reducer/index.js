import { createContext } from 'react'

export const store = {
  num:0
}

export const reducer = (state,action)=>{
  switch (action.type) {
    case 'changeCount':
      return {
        ...state,
        num:action.num
      }
    default:
      break;
  }
}

const AppContext = createContext(store)

export default AppContext