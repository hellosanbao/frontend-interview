import { BrowserRouter,Route,Routes } from 'react-router-dom'

import Home from './views/Home'


export default function RouteComp(props){
  return (
    <BrowserRouter basename='/'>
      <Routes>
        <Route path='/home' element={<Home/>}/>
      </Routes>
    </BrowserRouter>
  )
}