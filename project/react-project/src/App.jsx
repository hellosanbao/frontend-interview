import TestComponent from './testComponent'
import RouteComp from './router'
import AppContext, { reducer, store } from './reducer/index'
import { useReducer } from 'react'
export default function App(){
  const [storeData,dispatch] = useReducer(reducer,store)
  return (
    <AppContext.Provider value={{storeData,dispatch}}>
      <TestComponent/>
      <RouteComp/>
    </AppContext.Provider>
  )
}