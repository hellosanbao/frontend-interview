import { useEffect, useState } from "react"

export default function useMytesthooks(){
  useEffect(()=>{
    console.log('useMytesthooks');
  },[])
  const [a,setA] = useState('a')
  return {
    a,
    setA
  }
}